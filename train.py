#!/usr/bin/env python3


import gym
import matplotlib.pyplot as plt
import torch

from itertools import count
from model import DQN
from model_utils import ReplayMemory, get_screen, optimize_model, select_action
from train_utils import plot_durations

if __name__ == '__main__':

    width_screen = 600
    env = gym.make('CartPole-v0').unwrapped
    device = torch.device('cpu')

    # training parameters
    BATCH_SIZE = 128
    GAMMA = 0.999
    EPS_START = 0.9  # decaying prob. of choosing a random action
    EPS_END = 0.05
    EPS_DECAY = 200
    TARGET_UPDATE = 10

    # load model to device
    policy_net = DQN().to(device)
    target_net = DQN().to(device)
    target_net.load_state_dict(policy_net.state_dict())
    target_net.eval()

    optimizer = torch.optim.RMSprop(policy_net.parameters())
    memory = ReplayMemory(10 ** 4)

    steps_trained = 0

    episode_durations = []

    num_episodes = 50
    for i_episode in range(num_episodes):
        # Initialize the environment and state
        env.reset()
        last_screen = get_screen(env=env, width_screen=width_screen, device=device)
        current_screen = get_screen(env=env, width_screen=width_screen, device=device)
        state = current_screen - last_screen
        for t in count():
            # Select and perform an action
            action, steps_trained = select_action(net=policy_net, state=state,
                                                  steps_trained=steps_trained,
                                                  greedy_threshold_start=EPS_START,
                                                  greedy_threshold_end=EPS_END,
                                                  greedy_threshold_decay=EPS_DECAY,
                                                  device=device)
            _, reward, done, _ = env.step(action.item())
            reward = torch.tensor([reward], device=device)

            # Observe new state
            last_screen = current_screen
            current_screen = get_screen(env=env, width_screen=width_screen,
                                        device=device)
            if not done:
                next_state = current_screen - last_screen
            else:
                next_state = None

            # Store the transition in memory
            memory.push(state, action, next_state, reward)

            # Move to the next state
            state = next_state

            # Perform one step of the optimization (on the target network)
            optimize_model(memory, batch_size=BATCH_SIZE, gamma=GAMMA,
                           policy_net=policy_net, target_net=target_net,
                           device=device, optimizer=optimizer)
            if done:
                episode_durations.append(t + 1)
                plot_durations(episode_durations=episode_durations)
                break
        # Update the target network, copying all weights and biases in DQN
        if i_episode % TARGET_UPDATE == 0:
            target_net.load_state_dict(policy_net.state_dict())

    print('Complete')
    env.render()
    env.close()
    plt.ioff()
    plt.show()
