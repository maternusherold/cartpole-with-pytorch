#!/usr/bin/env python3


import math
import numpy as np
import random
import torch
import torch.nn.functional as F

from collections import namedtuple
from PIL import Image
from torchvision import transforms as T

Transition = namedtuple('Transition', ('state', 'action',
                                       'next_action', 'reward'))

resize = T.Compose([T.ToPILImage(), T.Resize(40, interpolation=Image.CUBIC),
                    T.ToTensor()])


class ReplayMemory:
    def __init__(self, capacity):
        """Cyclic buffer holding recent transitions.

        Providing a size-limited cyclic buffer to replay recently observed
        transitions to enable experience replay. Sampling from such a buffer
        randomly was shown to stabilize the agent's training.

        :param capacity: buffer size
        """
        self.capacity = capacity
        self.memory = []
        self.position = 0

    def push(self, *args):
        """Saves transition in ring buffer. """
        # build buffer
        if len(self.memory) < self.capacity:
            self.memory.append(None)
        self.memory[self.position] = Transition(*args)
        # update position
        self.position = (self.position + 1) % self.capacity

    def sample(self, batch_size):
        """Pick a random sample from replay memory.

        :param batch_size: size of sample
        """
        return random.sample(self.memory, batch_size)

    def __len__(self):
        """Custom length function for replay memory. """
        return len(self.memory)


# FOLLOWING CODE FROM PYTORCH AND OPENAI
def get_cart_location(env, width_screen):
    """Computes middle of the cart. """
    width_world = env.x_threshold * 2
    scale = width_screen / width_world
    return int(env.state[0] * scale + width_screen / 2.0)


def get_screen(env, width_screen, device):
    """Returns a screen as single image. """
    screen = env.render('rgb_array').transpose((2, 0, 1))
    # remove top and bottom of screen
    screen = screen[:, 160:320]
    width_view = 320
    cart_location = get_cart_location(env=env, width_screen=width_screen)
    if cart_location < width_view // 2:
        slice_range = slice(width_view)
    elif cart_location > (width_screen - width_view // 2):
        slice_range = slice(-width_view, None)
    else:
        slice_range = slice(cart_location - width_view // 2,
                            cart_location + width_view // 2)
    # square resulting focus image
    screen = screen[:, :, slice_range]
    # rescale and convert to torch
    screen = np.ascontiguousarray(screen, dtype=np.float32) / 255.
    screen = torch.from_numpy(screen)
    # resize and add batch dimension
    return resize(screen).unsqueeze(0).to(device)


def select_action(net, state, steps_trained, greedy_threshold_start,
                  greedy_threshold_end, greedy_threshold_decay, device):
    sample = random.random()
    greedy_policy_threshold = greedy_threshold_end \
                              + (greedy_threshold_start - greedy_threshold_end) \
                              * math.exp(-1. * steps_trained / greedy_threshold_decay)
    steps_trained += 1
    if sample > greedy_policy_threshold:
        with torch.no_grad():
            return net(state).max(1)[1].view(1, 1), steps_trained
    else:
        return torch.tensor([[random.randrange(2)]], device=device,
                            dtype=torch.long), steps_trained


# from pytorch
def optimize_model(memory, batch_size, gamma, policy_net, target_net,
                   device, optimizer):
    if len(memory) < batch_size:
        return
    transitions = memory.sample(batch_size)
    # Transpose the batch (see https://stackoverflow.com/a/19343/3343043 for
    # detailed explanation). This converts batch-array of Transitions
    # to Transition of batch-arrays.
    batch = Transition(*zip(*transitions))

    # Compute a mask of non-final states and concatenate the batch elements
    # (a final state would've been the one after which simulation ended)
    non_final_mask = torch.tensor(tuple(map(lambda s: s is not None,
                                            batch.next_action)), device=device,
                                  dtype=torch.bool)
    non_final_next_states = torch.cat([s for s in batch.next_action
                                       if s is not None])
    state_batch = torch.cat(batch.state)
    action_batch = torch.cat(batch.action)
    reward_batch = torch.cat(batch.reward)

    # Compute Q(s_t, a) - the model computes Q(s_t), then we select the
    # columns of actions taken. These are the actions which would've been taken
    # for each batch state according to policy_net
    state_action_values = policy_net(state_batch).gather(1, action_batch)

    # Compute V(s_{t+1}) for all next states.
    # Expected values of actions for non_final_next_states are computed based
    # on the "older" target_net; selecting their best reward with max(1)[0].
    # This is merged based on the mask, such that we'll have either the expected
    # state value or 0 in case the state was final.
    next_state_values = torch.zeros(batch_size, device=device)
    next_state_values[non_final_mask] = target_net(non_final_next_states).max(1)[0].detach()
    # Compute the expected Q values
    expected_state_action_values = (next_state_values * gamma) + reward_batch

    # Compute Huber loss
    loss = F.smooth_l1_loss(state_action_values, expected_state_action_values.unsqueeze(1))

    # Optimize the model
    optimizer.zero_grad()
    loss.backward()
    for param in policy_net.parameters():
        param.grad.data.clamp_(-1, 1)
    optimizer.step()
