# Trying PyTorch on the Cartpole example

getting first experience with the PyTorch framework using the cartpole example. 
To solve the world a simple DQN is utilized learning from visual cues 
(pictures of the world). 

This repo is used to split the program to solve the world into smaller, 
connected, chunks fostering a better understanding of the workflow.

*The idea and approach was not developed by me and is mainly inspired by this 
[PyTorch post](https://pytorch.org/tutorials/intermediate/reinforcement_q_learning.html
).*

![cartpole world](figures/cartpole.jpg "Image of cartpole world")
