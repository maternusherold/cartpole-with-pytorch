#!/usr/bin/env python3


import torch


class DQN(torch.nn.Module):
    def __init__(self):
        super(DQN, self).__init__()
        self.conv1 = torch.nn.Conv2d(in_channels=3, out_channels=16,
                                     kernel_size=5, stride=2)
        self.batch1 = torch.nn.BatchNorm2d(16)
        self.conv2 = torch.nn.Conv2d(in_channels=16, out_channels=32,
                                     kernel_size=5, stride=2)
        self.batch2 = torch.nn.BatchNorm2d(32)
        self.conv3 = torch.nn.Conv2d(in_channels=32, out_channels=32,
                                     kernel_size=5, stride=2)
        self.batch3 = torch.nn.BatchNorm2d(32)
        self.linear = torch.nn.Linear(448, 2)

    def forward(self, x):
        """Compute forward pass using ReLu  activation and linear. """
        x = torch.nn.functional.relu(self.batch1(self.conv1(x)))
        x = torch.nn.functional.relu(self.batch2(self.conv2(x)))
        x = torch.nn.functional.relu(self.batch3(self.conv3(x)))
        return self.linear(x.view(x.size(0), -1))

